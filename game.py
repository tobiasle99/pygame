import pygame
from sys import exit

def display_score():
    current_time = (pygame.time.get_ticks() - start_time)/1000
    score_surf = Pixeltype_font.render(f'Score: {current_time}', False, 'Black')
    score_rect = score_surf.get_rect(center = (400, 50))
    screen.blit(score_surf, score_rect)
    return current_time

pygame.init()
screen = pygame.display.set_mode((800, 400))
pygame.display.set_caption('MOJE HRA XD')
clock = pygame.time.Clock()
Pixeltype_font = pygame.font.Font('font/Pixeltype.ttf', 50)
game_active = False
start_time =0/1000
sky_surface = pygame.image.load('graphics/Sky.png').convert()
ground_surface = pygame.image.load('graphics/ground.png').convert()





snail_surface = pygame.image.load('graphics/snail/snail1.png').convert_alpha()
snail_rect = snail_surface.get_rect(midbottom = (800,300))


player_surf = pygame.image.load('graphics/Player/player_walk_1.png').convert_alpha()
player_rect = player_surf.get_rect(midbottom = (80,300))
player_gravity = 0

player_stand = pygame.image.load('graphics/Player/player_stand.png').convert_alpha()
player_stand_scaled = pygame.transform.rotozoom(player_stand, 0, 2)
player_stand_rect = player_stand_scaled.get_rect(center = (400, 200))

game_name = Pixeltype_font.render('Miss Me With That Snail Shit', False, (111,196,169))
game_name_rect = game_name.get_rect(center = (400,80))

game_message = Pixeltype_font.render('Press Spacebar to Start', False, (111,196,169))
game_message_rect = game_message.get_rect(center = (400,330))

score = 0
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if game_active:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if player_rect.bottom >= 300:
                        player_gravity = -20
        else:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    game_active = True
                    snail_rect.left = 800
                    start_time = pygame.time.get_ticks()
                    
                    
        
    if game_active:
        
        screen.blit(sky_surface, (0,0))
        screen.blit(ground_surface, (0, 300))
        screen.blit(snail_surface, snail_rect)
        score = display_score()
        
        if snail_rect.left > 0:
            snail_rect.left -= 10
        else:
            snail_rect.left = 800
            
        
        
        player_gravity += 1
        player_rect.y += player_gravity
        if player_rect.bottom >= 300:
            player_rect.bottom = 300


        screen.blit(player_surf, player_rect)

        if (player_rect.colliderect(snail_rect)):
            game_active = False
            
        
    else:
        screen.fill((94,129,162))
        screen.blit(player_stand_scaled, player_stand_rect)
        screen.blit(game_name, game_name_rect)
        if score == 0:
            screen.blit(game_message, game_message_rect)
        else:
            score_message = Pixeltype_font.render(f'Your score: {score}', False, (111,196,169))
            score_message_rect = score_message.get_rect(center = (400,330))
            screen.blit(score_message, score_message_rect)
    pygame.display.update()
    clock.tick(60)